/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package main

import (
    "bufio"
    "flag"
    "fmt"
    "gopkg.in/ini.v1"
    "io"
    "io/ioutil"
    "log"
    "os"
    "strings"
)

func main() {
    flag.Usage = func() {
        fmt.Fprintf(flag.CommandLine.Output(),
            "Usage: iniq [-h] [-i FILE] SECTION[.SECTION...].KEY\n")
        flag.PrintDefaults()
    }
    var inFile = flag.String("i", "-", "the file to read from; defaults to STDIN")
    flag.Parse()
    if len(flag.Args()) != 1 {
        flag.Usage()
        os.Exit(4)
    }
    path := flag.Arg(0)
    parts := strings.Split(path, ".")
    if len(parts) < 2 {
        flag.Usage()
        os.Exit(5)
    }
    section := strings.Join(parts[:len(parts) - 1], ".")
    key := parts[len(parts) - 1]

    var reader io.Reader
    if *inFile == "-" {
        reader = bufio.NewReader(os.Stdin)
    } else {
        f, err := os.Open(*inFile)
        if err != nil {
            log.Printf("%v", err)
            os.Exit(6)
        }
        reader = bufio.NewReader(f)
    }
    cfg, err := ini.Load(ioutil.NopCloser(reader))
    if err != nil {
        log.Printf("%v", err)
        os.Exit(1)
    }

    s, err := cfg.GetSection(section)
    if err != nil {
        log.Printf("%v", err)
        os.Exit(2)
    }
    k, err := s.GetKey(key)
    if err != nil {
        log.Printf("%v", err)
        os.Exit(3)
    }

    fmt.Println(k.String())
}
