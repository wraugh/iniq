iniq
====

Command-line tool to print a single value from an INI file

Getting started
---------------

Install iniq:

```
$ go get gitlab.com/wraugh/iniq
```

Read some values from a file:

```
$ cat example.ini
[beatrix]
codename = "Black Mamba"
location = unknown
[elle]
codename = "California Mountain Snake"
location = "El Paso, TX"
$ iniq beatrix.codename < example.ini
Black Mamba
$ iniq beatrix.location < example.ini
unknown
```
